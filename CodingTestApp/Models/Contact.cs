﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodingTestApp.Models
{
    public class Contact
    {
        public Contact()
        {

        }
        public int ID { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Email { get; set; }
        public String Address { get; set; }
        public Double? Longitude { get; set; }
        public Double? Latitude { get; set; }
    }
}