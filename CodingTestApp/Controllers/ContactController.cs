﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CodingTestApp.Models;
using CodingTestApp.DataContext;

namespace CodingTestApp.Controllers
{
    public class ContactController : Controller
    {
        private ContactContext db = new ContactContext();


        // GET: /Contact/

        //public ActionResult Index()
        //{
        //    return View(db.Contacts.ToList());
        //}

        public ActionResult Index(String Field = "", String Value = "")
        {
            if (Field == "" || Value == "")
            {
                return View(db.Contacts.ToList());
            }
            else
            {
                IQueryable<Contact> contacts = db.Contacts;
                switch (Field)
                {
                    case "First":
                        contacts = from c in db.Contacts
                                   where c.FirstName.Contains(Value)
                                   select c;
                        break;
                    case "Last":
                        contacts = from c in db.Contacts
                                   where c.LastName.Contains(Value)
                                   select c;
                        break;
                    case "Email":
                        contacts = from c in db.Contacts
                                   where c.Email.Contains(Value)
                                   select c;
                        break;
                    case "Address":
                        contacts = getContactsByDistance(contacts.ToList(), Value);
                            break;
                    default:
                        break;
                }
                return View(contacts.ToList());
            }
        }

        private IQueryable<Contact> getContactsByDistance(List<Contact> contacts, String coords)
        {
            String[] words = coords.Split(',');
            double latitude = Convert.ToDouble(words[0]);
            double longitude = Convert.ToDouble(words[1]);
            List<Contact> list = new List<Contact>();
            double minDist = -1;

            //Contact[] minDistList = new Contact[contacts.ToArray().Length];

            for (var i = 0; i < contacts.Count; i++)
            {
                if (!String.IsNullOrEmpty(contacts[i].Address))
                {
                    var dist = Math.Abs((contacts[i].Longitude.Value - longitude))
                        + Math.Abs(contacts[i].Latitude.Value - latitude);

                    if (minDist == -1)
                    {
                        minDist = dist;
                        list.Add(contacts[i]);
                    }
                    else if (dist < minDist)
                    {
                        list.Clear();
                        list.Add(contacts[i]);
                        minDist = dist;
                    }
                    else if (dist == minDist)
                    {
                        list.Add(contacts[i]);
                    }
                }
            }

            return list.AsQueryable();
        }


        // GET: /Contact/Details/5

        public ActionResult Details(int id = 0)
        {
            Contact contact = db.Contacts.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        //
        // GET: /Contact/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Contact/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Contact contact)
        {
            if (ModelState.IsValid)
            {
                db.Contacts.Add(contact);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(contact);
        }

        //
        // GET: /Contact/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Contact contact = db.Contacts.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        //
        // POST: /Contact/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Contact contact)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contact).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(contact);
        }

        //
        // GET: /Contact/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Contact contact = db.Contacts.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        //
        // POST: /Contact/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Contact contact = db.Contacts.Find(id);
            db.Contacts.Remove(contact);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}