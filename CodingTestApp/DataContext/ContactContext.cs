﻿using CodingTestApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CodingTestApp.DataContext
{
    public class ContactContext : DbContext
    {
        public ContactContext()
            : base("ContactContext")
        {

        }

        public DbSet<Contact> Contacts { get; set; }
    }
}